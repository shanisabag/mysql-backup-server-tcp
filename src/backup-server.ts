import log from "@ajar/marker";
import net from "net";
import { exec } from "child_process";
import cron from "node-cron";
import databases from "../config.json";

const obj_db = JSON.parse(JSON.stringify(databases));

const socket = net.createConnection({ port: 8124 }, () => {
    log.cyan("✨✨ backup-server connected to storage-server! ✨✨");
    for (const key in obj_db) {
        // Schedule tasks to be run on the server.
        cron.schedule("*/30 * * * * *", () => {
            const task = exec(
                `docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty ${key}`
            );
    
            const fileName = `${Date.now()}${obj_db[key]}`;

            task.stdout?.on("data", data => {
                socket.write(`${JSON.stringify({data: data, file_name: fileName})}\n`);
            });

            log.green("running a task every 30 sec");
        });
    }
});

socket.on("error", err => log.error(err));

socket.on("end", () => { 
    log.yellow("disconnected from storage-server");
 });
